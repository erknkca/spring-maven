

<%@page import="java.util.Date"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    Logger logger = Logger. 
            getLogger("index"); 
     Date simdikiZaman = new Date();
    logger.log(Level.INFO, "index.jsp,user ip :"+request.getRemoteAddr() 
            +" time :" + simdikiZaman.toString()); 
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Bootstrap  Form</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="styles.css" rel="stylesheet">
	</head>
	<body>
            <div style="color: #FF0000;">${errorMessage}</div>
<!--login modal-->
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          
          <h1 class="text-center">Upload Image</h1>
      </div>
      <div class="modal-body">
          
          <form class="form col-md-12 center-block" action="UploaderServlet" method="post" enctype="multipart/form-data" >
            <div class="form-group">
              <input type="text" class="form-control input-lg" placeholder="Introduction" name="intro" >
            </div>
            <div class="form-group">
              <input type="file" name="file" class="form-control input-lg" >
            </div>
            
          <div class="form-group">
              <input type="submit" name="submit" value="Submit">
          
		
      </div>
          </form>
          <div class="modal-footer"></div>
      </div>
      
  </div>
  </div>
</div>
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="bootstrap.min.js"></script>
	</body>
        
</html>