# README #

### Spring MVC Project ###

* Spring MVC ile resim ekleme ve gösterimi projesi
* Maven Projesi oluşturuldu
* Resimler için maksimum dosya büyüklüğü kontrolü yapılır
* Bootstrap js ile Listeden tıklanan resim tam ekran görüntülenir
* Logger ile sayfa görüntüleme yapan Client IP adresi ve görüntülenme zamani loglanmaktadır.
* Uygulamaya en fazla 50 tane dosya yüklenebilir. 50 sayısına ulaşılınca en büyük boyutlu dosya silinir