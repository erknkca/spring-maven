<%-- 
    Document   : show
    Created on : 02.Kas.2015, 23:33:09
    Author     : erkankoca
--%>

<%@page import="java.util.Date"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.io.File"%>
<%@page import="com.erkan.common.dao.PictureDAO"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.context.ConfigurableApplicationContext"%>
<%@page import="com.erkan.common.model.Picture" %>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    Logger logger = Logger. 
            getLogger("show"); 
    
     Date simdikiZaman = new Date();
            
    logger.log(Level.INFO, "show.jsp,user ip :"+request.getRemoteAddr() 
            +" time :" + simdikiZaman.toString()); 

    
    
    @SuppressWarnings("resource")
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        PictureDAO dao = (PictureDAO) context.getBean("pictureDAO");
        List<Picture> pictures = dao.getAll();
        
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Bootstrap Show Form</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/lightbox.css" rel="stylesheet" media="screen" />
                
                <script src="//code.jquery.com/jquery-2.x.x.min.js"></script>
                        
		<link href="css/styles.css" rel="stylesheet">
               
                <script src="js/lightbox.js"></script>
               
                     
                 
	</head>
	<body>
               
<!--login modal-->
<div id="loginModal" class="modal-open" tabindex="-1" aria-hidden="true">
  <div class="modal-body">
  <div class="modal-content">
      <div class="modal-header">
          
          <h1 class="text-center">Show Image and Comments</h1>
      </div>
      <div class="modal-body text-center">
 
          <%
        for(Picture picture : pictures) {
            
          %>
          
          
          <form class="form text-center" >
              
            <div class="form-group">
                   
                <a href="<%=picture.getName() %>" data-lightbox="<%=picture.getName() %>" data-title="My caption" >
                    <img src="<%=picture.getName() %> " alt=""></a>
    
                   
                <label class="text-center"> <%=picture.getName() %> </label> 
            </div>
            <div class="form-group">
              <label class="text-center"> <%=picture.getComment() %> </label> 
            </div>
            
          <div class="form-group">
              <label class="text-center"> <%=picture.getPTime() %> </label> 
          
		
      </div>
          </form>
          <% 
        }%>
     
      </div>
      
  </div>
  </div>
</div>
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
