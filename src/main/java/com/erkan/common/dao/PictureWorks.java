/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erkan.common.dao;

import com.erkan.common.model.Picture;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;


/**
 *
 * @author erkankoca
 */
public class PictureWorks implements PictureDAO {

    private DataSource dataSource;
 
    public void setDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    @Override
    public void add(Picture picture) {
    
           
        //Mysql veri tabanına resim kayıt eden fonksiyon
        
        String sql = "INSERT INTO picture " +
				"(name,comment,ptime,size) VALUES (?, ?, ?, ?)";
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, picture.getName());
                        ps.setString(2, picture.getComment());
			ps.setString(3, picture.getPTime());
                        ps.setInt(4,picture.getSize());
			ps.executeUpdate();
                        
			ps.close();
			
                        
		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
    
    }

    @Override
    public List<Picture> getAll() {
    
    
        //Veri tabanındaki resimleri listeleyerek döndüren fonksiyon
        
        
        String sql = "SELECT * FROM picture ";
        List<Picture> pictures = new ArrayList<Picture>();
		
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			Picture pic = null;
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				pic = new Picture();
				pic.setName(rs.getString("name")); 
				pic.setComment(rs.getString("comment"));
                                pic.setPTime(rs.getString("ptime"));
                                pictures.add(pic);
				
			}
			rs.close();
			ps.close();
			return pictures;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
    
    }

    int count;
    @Override
    public int counter() {
    
    
        // Veri tabanında kayıtlı resim sayısını döndüren fonksiyon
        
        String sql = "SELECT count(id) FROM picture ";
        	
        Connection conn = null;

        try {
                conn = dataSource.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql);

                ResultSet rs = ps.executeQuery();
                if (rs.next()) {

                       count = rs.getInt("count(id)");	
                }
                rs.close();
                ps.close();
                return count;
        } catch (SQLException e) {
                throw new RuntimeException(e);
        } finally {
                if (conn != null) {
                        try {
                        conn.close();
                        } catch (SQLException e) {}
                }
        }
    
    }

    @Override
    public void deletePic() {
    
    
       // veri tabanındaki boyutu en büyük olan dosyayı bulan ve kayıdını silen fonksiyon
    
        String sql = "SELECT id from picture order by size desc LIMIT 0,1 ";
        	
        Connection conn = null;
        
        int deleteId = 0;

        try {
                conn = dataSource.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql);

                ResultSet rs = ps.executeQuery();
                if (rs.next()) {

                       deleteId = rs.getInt("id");	
                }
                rs.close();
                ps.close();
                
        } catch (SQLException e) {
                throw new RuntimeException(e);
        } finally {
                if (conn != null) {
                        try {
                        conn.close();
                        } catch (SQLException e) {}
                }
        }
        
        String delsql = "delete from picture where id=?";
        try {
                conn = dataSource.getConnection();
                PreparedStatement ps = conn.prepareStatement(delsql);
                
                ps.setInt(1, deleteId);

                ps.executeUpdate();
		ps.close();
                
        } catch (SQLException e) {
                throw new RuntimeException(e);
        } finally {
                if (conn != null) {
                        try {
                        conn.close();
                        } catch (SQLException e) {}
                }
        }
        
    }
    
}
