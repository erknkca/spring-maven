/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erkan.common.dao;

import com.erkan.common.model.Picture;
import java.util.List;

/**
 *
 * @author erkankoca
 */
public interface PictureDAO {
   
   //İmplemente edilecek fonksiyonları içeren Data Acces Object sınıfı
   
    public void add (Picture p);
    public List<Picture> getAll();
    public int counter();
    public void deletePic();
}
