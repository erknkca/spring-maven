/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erkan.common.model;

/**
 *
 * @author erkankoca
 */
public class Picture {
    
    private int pid;
    private String pname;
    private String comment;
    private String ptime;
    private int size;
    
    //resim hakkında blgileri tutmaya yarayan bean sınıfı
 
    public void setPid(int id ) {
        this.pid = id;     
    }
    public int getPid() {
        return this.pid;
    }
    
    public String getName() {
        return this.pname;
    }
    public void setName(String name) {
        this.pname = name;
    }
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment=comment;
    }
 
    public String getPTime(){
        return this.ptime;
    } 
    
    public void setPTime(String time){
        this.ptime = time;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    
    
}
