/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erkan.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.erkan.common.dao.PictureDAO;
import com.erkan.common.model.Picture;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Date;
import org.springframework.stereotype.Controller;




/**
 *
 * @author erkankoca
 */
@Controller
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)
public class UploaderServlet extends HttpServlet {

int maxFileSize=1024*1024*10;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        
        Logger logger = Logger. 
            getLogger("com.java.FileUploadServlet"); 
         Date simdikiZaman = new Date();
    logger.log(Level.INFO, "FileUploadServlet,user :"+request.getRemoteAddr() 
            +" time :" + simdikiZaman.toString()); 
            
            // Client IP bilgisi ve görüntüleme zamanı loglanıyor
    
        
        response.setContentType("text/html;charset=UTF-8");
            
            String intro=request.getParameter("intro");
            //acıklama bilgisi alınıyor
            Part part=request.getPart("file");
            //dosya alınıyor
            
            String fileName=part.getSubmittedFileName();
            // dosya adı alınıyor
            int ssize = (int) request.getPart("file").getSize();
            // dosya boyutu alınıyor
            
            if(maxFileSize < ssize){
            //dosya boyutu kontrolü
            System.out.println( "Dosya çok büyük !");
            request.setAttribute("errorMessage", "Dosya boyutu çok büyük !!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
            
            @SuppressWarnings("resource")
            ConfigurableApplicationContext context = 
                    new ClassPathXmlApplicationContext("context.xml");
            PictureDAO dao = (PictureDAO) context.getBean("pictureDAO");
            Picture pic = new Picture();
            
            
            int number = dao.counter();
            //kayıtlı dosya sayısı alınıyor
            
            if (number == 50){
            
                dao.delelePic();
                //dosya sayısı 50 olmus ise boyutu en büyük dosyayı silen fonksiyon cagrılır
            
            }
            
            // hersey uygunsa veri tabanına kayıt kayıt yapılır
            pic.setName(fileName);
            pic.setPTime(simdikiZaman.toString());
            pic.setComment(intro);
            pic.setSize(sszie);
            dao.add(pic);
            
           
            //dosya, belirlenen proje yoluna gönderilir.
            File uploads = new File(getServletContext().getRealPath(""));
            File file = new File(uploads, fileName);

            try (InputStream input = part.getInputStream()) {
                Files.copy(input, file.toPath());
            }
            
        // son olarak resim listelendiği sayfaya yönlendirme yapılır
        response.sendRedirect("show.jsp");
           
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UploaderServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UploaderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UploaderServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UploaderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
