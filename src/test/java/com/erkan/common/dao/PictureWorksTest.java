/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erkan.common.dao;

import com.erkan.common.model.Picture;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author erkankoca
 */
public class PictureWorksTest {


    //Junit test class
    
    public PictureWorksTest() {
    }

    /**
     * Test of add method, of class PictureWorks.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Picture pic = new Picture();
        pic.setName("dosyaAdi");
        pic.setComment("blabla");
        pic.setSize(1515);
        Date simdikiZaman = new Date();
        pic.setPTime(simdikiZaman.toString());
        PictureWorks instance = new PictureWorks();
        instance.add(pic);
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAll method, of class PictureWorks.
     */
    @Test
    public void testGetAll() {
        System.out.println("getAll");
        PictureWorks instance = new PictureWorks();
        Picture pic1 = new Picture();
        pic1.setName("dosyaAdi");
        pic1.setComment("blabla");
        pic1.setSize(1515);
        Date simdikiZaman = new Date();
        pic1.setPTime(simdikiZaman.toString());
        Picture pic2 = new Picture();
        pic2.setName("dosyaAdi");
        pic2.setComment("blabla");
        pic2.setSize(1515);
        Date Zaman = new Date();
        pic2.setPTime(Zaman.toString());
        
        List<Picture> expResult = new ArrayList<Picture>();
        expResult.add(pic1);
        expResult.add(pic2);
        List<Picture> result = instance.getAll();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of counter method, of class PictureWorks.
     */
    @Test
    public void testCounter() {
        System.out.println("counter");
        PictureWorks instance = new PictureWorks();
        int expResult = 7; // işlem yapıldığı anda kayıt sayısı:7 idi
        int result = instance.counter();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of deletePic method, of class PictureWorks.
     */
    @Test
    public void testDeletePic() {
        System.out.println("deletePic");
        PictureWorks instance = new PictureWorks();
        instance.deletePic();
        fail("The test case is a prototype.");
    }
    
}
